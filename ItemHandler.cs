namespace csharpcore
{
    public interface ItemHandler
    {
        bool CanHandle(Item Item);

        Item Handle(Item Item);
    }

    public class SulfurasHandler : ItemHandler
    {
        public bool CanHandle(Item Item)
        {
            return Item.Name == "Sulfuras, Hand of Ragnaros";
        }

        public Item Handle(Item Item)
        {
            return Item;
        }
    }

    public class AgedBrieHandler : ItemHandler
    {
        public bool CanHandle(Item Item)
        {
            return Item.Name == "Aged Brie";
        }

        public Item Handle(Item Item)
        {
            Item.SellIn--;

            Item.Quality = Item.Quality + (Item.SellIn >= 0 ? 1 : 2);

            if (Item.Quality > 50)
            {
                Item.Quality = 50;
            }

            return Item;
        }
    } 

    public class BackstagePassesHandler : ItemHandler
    {
        public bool CanHandle(Item Item)
        {
            return Item.Name == "Backstage passes to a TAFKAL80ETC concert";
        }

        public Item Handle(Item Item)
        {
            Item.SellIn--;

            if (Item.SellIn < 0)
            {
                Item.Quality = 0;
                return Item;
            }

            int Factor = 1;
            if (Item.SellIn < 10)
            {
                Factor++;
            }

            if (Item.SellIn < 5)
            {
                Factor++;
            }

            Item.Quality = Item.Quality + Factor;

            if (Item.Quality > 50)
            {
                Item.Quality = 50;
            }

            return Item;
        }
    }

    public class ConjuredHandler : ItemHandler
    {
        public bool CanHandle(Item Item)
        {
            return Item.Name == "Conjured Mana Cake";
        }

        public Item Handle(Item Item)
        {
            Item.SellIn--;

            Item.Quality = Item.Quality - (Item.SellIn >= 0 ? 2 : 4);

            if (Item.Quality < 0)
            {
                Item.Quality = 0;
            }

            return Item;
        }
    }

    public class OrdinaryHandler : ItemHandler
    {
        public bool CanHandle(Item Item)
        {
            return true;
        }

        public Item Handle(Item Item)
        {
            Item.SellIn--;

            Item.Quality = Item.Quality - (Item.SellIn >= 0 ? 1 : 2);

            if (Item.Quality < 0)
            {
                Item.Quality = 0;
            }

            return Item;
        }
    }
}