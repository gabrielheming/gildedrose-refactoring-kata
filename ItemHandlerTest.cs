using Xunit;
using System.Collections.Generic;

namespace csharpcore
{
    public class ItemHandlerTest
    {
        [Fact]
        public void SulfurasHandler()
        {
            Item item = new Item {Name = "Sulfuras, Hand of Ragnaros", SellIn = 2, Quality = 80};

            ItemHandler itemHandler = new SulfurasHandler();
            Assert.True(itemHandler.CanHandle(item));
            Assert.Equal(80 , item.Quality);
            Assert.Equal(2 , item.SellIn);

            item = itemHandler.Handle(item);

            Assert.Equal(80 , item.Quality);
            Assert.Equal(2 , item.SellIn);

            //Ordinary item
            Assert.False(itemHandler.CanHandle(new Item ()));

            for (int i = 0 ; i <= 100 ; i++)
            {
                item = itemHandler.Handle(item);
                Assert.Equal(80 , item.Quality);
                Assert.Equal(2 , item.SellIn);
            }
        }

        [Fact]
        public void OrdinaryHandler()
        {
            Item item = new Item {SellIn = 2, Quality = 50};

            ItemHandler itemHandler = new OrdinaryHandler();
            Assert.True(itemHandler.CanHandle(item));

            item = itemHandler.Handle(item);

            Assert.Equal(49 , item.Quality);
            Assert.Equal(1 , item.SellIn);
        }

        [Fact]
        public void AgedBrieHandler()
        {
            Item item = new Item {Name = "Aged Brie", SellIn = 1, Quality = 0};

            ItemHandler itemHandler = new AgedBrieHandler();
            Assert.True(itemHandler.CanHandle(item));

            Assert.Equal(0 , item.Quality);
            Assert.Equal(1 , item.SellIn);

            item = itemHandler.Handle(item);

            Assert.Equal(1 , item.Quality);
            Assert.Equal(0 , item.SellIn);

            item = itemHandler.Handle(item);

            Assert.Equal(3 , item.Quality);
            Assert.Equal(-1 , item.SellIn);
        }

        [Fact]
        public void BackstagePassesHandler()
        {
            Item item = new Item {Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 2, Quality = 45};

            ItemHandler itemHandler = new BackstagePassesHandler();
            Assert.True(itemHandler.CanHandle(item));

            Assert.Equal(45 , item.Quality);
            Assert.Equal(2 , item.SellIn);

            item = itemHandler.Handle(item);

            Assert.Equal(48 , item.Quality);
            Assert.Equal(1 , item.SellIn);

            item = itemHandler.Handle(item);

            Assert.Equal(50 , item.Quality);
            Assert.Equal(0 , item.SellIn);

            item = itemHandler.Handle(item);

            Assert.Equal(0 , item.Quality);
            Assert.Equal(-1 , item.SellIn);
        }

        [Fact]
        public void ConjuredHandler()
        {
            Item item = new Item {Name = "Conjured Mana Cake", SellIn = 2, Quality = 20};

            ItemHandler itemHandler = new ConjuredHandler();
            Assert.True(itemHandler.CanHandle(item));

            Assert.Equal(20 , item.Quality);
            Assert.Equal(2 , item.SellIn);

            item = itemHandler.Handle(item);

            Assert.Equal(18 , item.Quality);
            Assert.Equal(1 , item.SellIn);

            item = itemHandler.Handle(item);

            Assert.Equal(16 , item.Quality);
            Assert.Equal(0 , item.SellIn);

            item = itemHandler.Handle(item);

            Assert.Equal(12 , item.Quality);
            Assert.Equal(-1 , item.SellIn);
        }
    }
}