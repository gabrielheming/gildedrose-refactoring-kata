﻿using Xunit;
using System.Collections.Generic;

namespace csharpcore
{
    public class GildedRoseTest
    {
        [Fact]
        public void QualityValuesRange()
        {
            IList<Item> Items = new List<Item> { 
                new Item {Name = "+5 Dexterity Vest", SellIn = 10, Quality = 20},
                new Item {Name = "Aged Brie", SellIn = 2, Quality = 0},
                new Item {Name = "Elixir of the Mongoose", SellIn = 5, Quality = 7},
                new Item {Name = "Sulfuras, Hand of Ragnaros", SellIn = 0, Quality = 80},
                new Item {Name = "Sulfuras, Hand of Ragnaros", SellIn = -1, Quality = 80},
                new Item
                {
                    Name = "Backstage passes to a TAFKAL80ETC concert",
                    SellIn = 15,
                    Quality = 20
                },
                new Item
                {
                    Name = "Backstage passes to a TAFKAL80ETC concert",
                    SellIn = 10,
                    Quality = 49
                },
                new Item
                {
                    Name = "Backstage passes to a TAFKAL80ETC concert",
                    SellIn = 5,
                    Quality = 49
                },
				// this conjured item does not work properly yet
				new Item {Name = "Conjured Mana Cake", SellIn = 3, Quality = 6}
            };
            GildedRose app = new GildedRose(Items);

            for (var i = 0; i < 31; i++)
            {
                for (var j = 0; j < Items.Count; j++)
                {
                    this.AssertQualityRange(Items[j]);
                }
                app.UpdateQuality();
            }
        }

        private void AssertQualityRange(Item Item)
        {
            int GreatestQuality = Item.Name == "Sulfuras, Hand of Ragnaros" ? 80 : 50;

            Assert.True(Item.Quality >= 0 , "The Quality was not greater or equal than 0");
            Assert.True(Item.Quality <= GreatestQuality , string.Format("The Quality ulfuras, Hand of Ragnaros was not lesser or equal than {0}" , GreatestQuality));
        }

        [Fact]
        public void QualityDrop()
        {
            IList<Item> Items = new List<Item> { 
                new Item {Name = "+5 Dexterity Vest", SellIn = 1, Quality = 20},
                new Item {Name = "Aged Brie", SellIn = 1, Quality = 0},
                new Item {Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 1, Quality = 45},
				new Item {Name = "Conjured Mana Cake", SellIn = 1, Quality = 20}
            };

            GildedRose app = new GildedRose(Items);

            //Day 0 , SellIn 1
            Assert.Equal(20 , Items[0].Quality);
            Assert.Equal(0 , Items[1].Quality);
            Assert.Equal(45 , Items[2].Quality);
            Assert.Equal(20 , Items[3].Quality);
            
            app.UpdateQuality();

            //Day 1 , SellIn 0
            Assert.Equal(19 , Items[0].Quality);
            Assert.Equal(1 , Items[1].Quality);
            Assert.Equal(48 , Items[2].Quality);
            Assert.Equal(18 , Items[3].Quality);

            app.UpdateQuality();

            //Day 2 , SellIn -1
            Assert.Equal(17 , Items[0].Quality);
            Assert.Equal(3 , Items[1].Quality);
            Assert.Equal(0 , Items[2].Quality);
            Assert.Equal(14 , Items[3].Quality);

            app.UpdateQuality();

            //Day 3 , SellIn -2
            Assert.Equal(15 , Items[0].Quality);
            Assert.Equal(5 , Items[1].Quality);
            Assert.Equal(0 , Items[2].Quality);
            Assert.Equal(10 , Items[3].Quality);
        }
    }
}