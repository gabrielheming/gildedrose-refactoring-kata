=======================================================

Gabriel Heming - Gilded Rose Requirements Specification

=======================================================

Hi and welcome to Gabriel Heming Gilded Rose Refactored. This project has been developed on Visual Studio code. 

To execute this project on Visual Studio Code you have to:

    - Clone this repository;
    - Open the folder on Visual Studio Code;
        - File -> Open Folder [CTRL+K CTRL+O];
    - Run the command "dotnet run" on terminal to run Program.cs;
    - Run the command "dotnet test" on terminal to run all tests.
    
Visual Studio Code has to have the following extensions installed:
    - C# - Version 1.18.0
    - .NET Core Test Explorer 0.6.5.
    
This project also can be executed on regular Visual Studio, it just must has .NET Core SDK installed.