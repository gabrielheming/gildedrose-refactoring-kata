using Xunit;
using System.Collections.Generic;

namespace csharpcore
{
    public class QualityManagerTest
    {
        [Fact]
        public void QualityManager()
        {
            //Singleton
            QualityManager qm = csharpcore.QualityManager.Instance;

            Item sulfurasItem = new Item {Name = "Sulfuras, Hand of Ragnaros", SellIn = 2, Quality = 80};
            Item ordinaryItem = new Item {SellIn = 2, Quality = 50};
            Item agedBrieItem = new Item {Name = "Aged Brie", SellIn = 2, Quality = 0};
            Item backStagePassesItem = new Item {Name = "Backstage passes to a TAFKAL80ETC concert", SellIn = 2, Quality = 30};
            Item conjuredItem = new Item {Name = "Conjured Mana Cake", SellIn = 2, Quality = 20};

            qm.Update(sulfurasItem);
            Assert.Equal(80 , sulfurasItem.Quality);
            Assert.Equal(2 , sulfurasItem.SellIn);

            qm.Update(ordinaryItem);
            Assert.Equal(49 , ordinaryItem.Quality);
            Assert.Equal(1 , ordinaryItem.SellIn);

            qm.Update(agedBrieItem);
            Assert.Equal(1 , agedBrieItem.Quality);
            Assert.Equal(1 , agedBrieItem.SellIn);

            qm.Update(backStagePassesItem);
            Assert.Equal(33 , backStagePassesItem.Quality);
            Assert.Equal(1 , backStagePassesItem.SellIn);

            qm.Update(conjuredItem);
            Assert.Equal(18 , conjuredItem.Quality);
            Assert.Equal(1 , conjuredItem.SellIn);

            for (int i = 0 ; i <= 100 ; i++)
            {
                qm.Update(sulfurasItem);
                Assert.Equal(80 , sulfurasItem.Quality);
                Assert.Equal(2 , sulfurasItem.SellIn);

                qm.Update(ordinaryItem);

                int quality = 48 - (ordinaryItem.SellIn < 0 ? i * 2 : i);
                Assert.Equal(quality < 0 ? 0 : quality , ordinaryItem.Quality);
                Assert.Equal(-i , ordinaryItem.SellIn);

                qm.Update(agedBrieItem);
                quality = 2 + (ordinaryItem.SellIn < 0 ? i * 2 : i);
                Assert.Equal(quality > 50 ? 50 : quality , agedBrieItem.Quality);
                Assert.Equal(-i , agedBrieItem.SellIn);

                qm.Update(backStagePassesItem);
                quality = 36 + (backStagePassesItem.SellIn < 11 ? (backStagePassesItem.SellIn < 6 ? i * 3 : i * 2) : i);
                Assert.Equal( backStagePassesItem.SellIn < 0 ? 0 : quality , backStagePassesItem.Quality);
                Assert.Equal(-i , backStagePassesItem.SellIn);

                qm.Update(conjuredItem);
                quality = 16 - (ordinaryItem.SellIn < 0 ? i * 4 : i * 2);
                Assert.Equal(quality < 0 ? 0 : quality , conjuredItem.Quality);
                Assert.Equal(-i , conjuredItem.SellIn);
            }
        }
    }
}