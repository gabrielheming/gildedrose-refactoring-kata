using System.Collections.Generic;
using System.Linq;

namespace csharpcore
{
    public class QualityManager 
    {
        private List<ItemHandler> Handlers = new List<ItemHandler>();
    
        //Private Constructor.  
        private QualityManager()  
        {  

        }  

        private static QualityManager instance = null;  
        public static QualityManager Instance  
        {  
            get  
            {  
                if (instance == null)  
                {  
                    instance = new QualityManager();  
                    instance.AddHandler(new SulfurasHandler())
                            .AddHandler(new AgedBrieHandler())
                            .AddHandler(new BackstagePassesHandler())
                            .AddHandler(new ConjuredHandler())
                            
                            //Must be the last one
                            .AddHandler(new OrdinaryHandler());
              
                }  
                return instance;  
            }  
        }  

        public Item Update(Item Item)
        {
            ItemHandler itemHandler = this.Handlers.FirstOrDefault(_ => _.CanHandle(Item));

            return itemHandler.Handle(Item);
        }
    
        public QualityManager AddHandler(ItemHandler ItemHandler) 
        {
            this.Handlers.Add(ItemHandler);
            
            return this;
        }
    }
}